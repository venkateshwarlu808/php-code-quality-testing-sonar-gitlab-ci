<?php

function getUserData($id) {
    $conn = mysqli_connect("localhost", "root", "", "test_db");
    if (!$conn) {
        die("Connection failed");
    }

    $query = "SELECT * FROM users WHERE id = $id";
    $result = mysqli_query($conn, $query);

    if ($result) {
        return mysqli_fetch_assoc($result);
    } else {
        return "Error fetching data";
    }
}

function calculateDiscount($price, $quantity, $discount) {
    if ($quantity < 1) {
        return 0;
    }
    return $price * $quantity - $discount;
}

$data = getUserData(1);
print_r($data);

echo "Total price: " . calculateDiscount(100, 2, 10);

?>
