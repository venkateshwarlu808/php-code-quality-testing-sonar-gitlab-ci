import requests

def fetch_user_data(user_id, base_url="https://example.com/api/users"):
    """
    Fetch user data from the API.
    
    Args:
        user_id (int): The ID of the user.
        base_url (str): The base URL of the API.
    
    Returns:
        dict: User data if successful.
        str: Error message if the request fails.
    """
    try:
        response = requests.get(f"{base_url}/{user_id}")
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        return {"error": str(e)}

def calculate_total_price(price, quantity, tax_rate):
    """
    Calculate the total price including tax.
    
    Args:
        price (float): The unit price of the item.
        quantity (int): The number of items.
        tax_rate (float): The tax rate as a decimal.
    
    Returns:
        float: The total price including tax.
    """
    if quantity <= 0:
        return 0
    subtotal = price * quantity
    total = subtotal + (subtotal * tax_rate)
    return total

# Example usage
if __name__ == "__main__":
    user_data = fetch_user_data(123)
    print(user_data)
    
    total_price = calculate_total_price(100, 5, 0.1)
    print(f"Total Price: {total_price}")
